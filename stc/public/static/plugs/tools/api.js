var $API = {
	captcha: {
		name: "生成验证码",
		post: async function(url, data){
			return await http.post(url, data);
		}
	},
	login: {
		name: "用户登陆",
		post: async function(url, data){
			return await http.post(url, data);
		}
	},
	menu: {
		name: "普通系统菜单",
		get: async function(url){
			return await http.get(url);
		}
	},
	lang: {
		name: "切换显示语言",
		get: async function(url, data){
			return await http.get(url, data);
		}
	},
	breadcrumb: {
		name: "获取面包屑",
		post: async function(url, data){
			return await http.post(url, data);
		}
	},
}