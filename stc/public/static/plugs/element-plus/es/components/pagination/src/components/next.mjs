import '../../../../utils/index.mjs';
import { buildProps } from '../../../../utils/vue/props/runtime.mjs';

const paginationNextProps = buildProps({
  disabled: Boolean,
  currentPage: {
    type: Number,
    default: 1
  },
  pageCount: {
    type: Number,
    default: 50
  },
  nextText: {
    type: String
  }
});

export { paginationNextProps };
//# sourceMappingURL=next.mjs.map
