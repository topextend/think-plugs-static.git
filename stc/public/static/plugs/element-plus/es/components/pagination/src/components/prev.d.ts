import type { ExtractPropTypes } from 'vue';
import type Prev from './prev.vue';
export declare const paginationPrevProps: {
    readonly disabled: BooleanConstructor;
    readonly currentPage: import("element-plus/es/utils").EpPropFinalized<NumberConstructor, unknown, unknown, 1, boolean>;
    readonly prevText: {
        readonly type: import("vue").PropType<string>;
        readonly required: false;
        readonly validator: ((val: unknown) => boolean) | undefined;
        __epPropKey: true;
    };
};
export declare const paginationPrevEmits: {
    click: (evt: MouseEvent) => boolean;
};
export declare type PaginationPrevProps = ExtractPropTypes<typeof paginationPrevProps>;
export declare type PrevInstance = InstanceType<typeof Prev>;
